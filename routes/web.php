<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@show');

Route::get('home', 'HomeController@show');

Route::resource("leads", 'LeadController');
Route::resource("lead_status", "LeadStatusController");
Route::resource("lead_forms", "LeadFormController");
Route::resource("lead_form_fields", "LeadFormFieldController");
Route::resource("field_values", "FieldValueController");

Route::get('render/form/{form}', 'FormRenderController@view');
Route::get('render/field/{type}', 'FormFieldRenderController@view');

Route::get('email/submission/{submission}', 'SendEmailsController@sendSubmission');
Route::post('leadform/settings/{form}', 'FormSettingsController@view');

Route::get('shopify', 'ShopifyController@index');
Route::get('shopify/register', 'ShopifyOAuthController@register');
Route::get('shopify/start_auth', 'ShopifyOAuthController@startAuth');
Route::get('shopify/authorise', 'ShopifyOAuthController@authorise');
Route::get('shopify/settings', 'ShopifyController@settings');
Route::get('shopify/help', 'ShopifyController@help');
