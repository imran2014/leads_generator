<!-- Left Side Of Navbar -->
<li>
  <a href="{{ url('leads') }}">Leads</a>
  {{-- Maybe manage the lead status from here? --}}
</li>
<li>
  <a href="{{ url('lead_forms') }}">Forms</a>
</li>
<li>
  <a href="{{ url('lead_form_fields') }}">Form Fields</a>
</li>
