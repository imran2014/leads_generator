@extends('spark::layouts.app-inner')

@section('header')
<div class="page-header clearfix">
  <h1>
    <i class="glyphicon glyphicon-align-justify"></i> Form Fields
    <a class="btn btn-success pull-right" href="{{ route('lead_form_fields.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
  </h1>

</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if($lead_form_fields->count())
    <table class="table table-condensed table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Label</th>
          <th>Field Type</th>
          <th>Weight</th>
          {{-- <th>SETTINGS</th> --}}
          <th class="text-right">OPTIONS</th>
        </tr>
      </thead>
      <tbody>
        @foreach($lead_form_fields as $lead_form_field)
        <tr>
          <td>{{$lead_form_field->id}}</td>
          <td>{{$lead_form_field->field_label}}</td>
          <td>{{$lead_form_field->field_type_human}}</td>
          <td>{{$lead_form_field->weight}}</td>
          {{-- <td><a href="#">View Settings</a></td> --}}
          <td class="text-right">
            <a class="btn btn-xs btn-primary" href="{{ route('lead_form_fields.show', $lead_form_field->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
            <a class="btn btn-xs btn-warning" href="{{ route('lead_form_fields.edit', $lead_form_field->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            <form action="{{ route('lead_form_fields.destroy', $lead_form_field->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $lead_form_fields->render() !!}
    @else
    <h3 class="text-center alert alert-info">Empty!</h3>
    @endif

  </div>
</div>
@endsection
