<label>CSS class</label>
<input type="text" class="form-control settings-field" data-form-setting-id="css-class">
<span class="help-block">Generate this checkbox with certain classes.</span>

<label>Placeholder</label>
<input type="text" class="form-control settings-field" data-form-setting-id="placeholder">
<span class="help-block">The default text in the field.</span>

<label>Number of rows</label>
<input type="text" class="form-control settings-field" data-form-setting-id="textarea-rows">
<span class="help-block">Size of textarea.</span>

@include ('lead_form_fields.fieldTypeSettings.partials.required')
