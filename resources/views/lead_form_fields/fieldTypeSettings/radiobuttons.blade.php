<label>Values</label>
<textarea class="form-control settings-field" rows=10 data-form-setting-id="values"></textarea>
<span class="help-block">Include the values of the checkboxes here, one line per value.</span>

<label>CSS class</label>
<input type="text" class="form-control settings-field" data-form-setting-id="css-class">
<span class="help-block">Generate this checkbox with certain classes.</span>
