<label>Values</label>
<textarea class="form-control settings-field" rows=10 data-form-setting-id="values"></textarea>
<span class="help-block">Include the values of the checkboxes here, one line per value.</span>

<label>CSS class</label>
<input type="text" class="form-control settings-field" data-form-setting-id="css-class">
<span class="help-block">Generate this checkbox with certain classes.</span>

<label>First Option in drop down</label>
<input type="text" class="form-control settings-field" data-form-setting-id="select-first">
<span class="help-block">This will be first option users see. Leave blank to set as "Please select". </span>

@include ('lead_form_fields.fieldTypeSettings.partials.required')
