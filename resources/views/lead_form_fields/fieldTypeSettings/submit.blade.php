<label>CSS class</label>
<input type="text" class="form-control settings-field" data-form-setting-id="css-class">
<span class="help-block">Generate this checkbox with certain classes.</span>

<label>Submit Value</label>
<input type="text" class="form-control settings-field" data-form-setting-id="submit-value">
<span class="help-block">This will be what the users see the button text as. Leave blank to make the same as the form field label above.</span>

