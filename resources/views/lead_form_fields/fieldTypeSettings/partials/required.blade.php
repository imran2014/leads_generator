<label>Required field?</label>
<div class="radio required-settings">
  <label>
    <input type="radio" name="required" value="1" class="settings-field" data-form-setting-checkbox-id="required"> Yes
  </label>
</div>
<div class="radio">
<label>
    <input type="radio" name="required" value="0" class="settings-field" data-form-setting-checkbox-id="required"> No
  </label>
</div>
<span class="help-block">This will set the field to <strong>required</strong> on the form. </span>
