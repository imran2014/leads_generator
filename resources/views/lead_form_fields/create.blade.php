@extends('spark::layouts.app-inner')
@section('css')
@endsection
@section('header')
<div class="page-header">
  <h1><i class="glyphicon glyphicon-plus"></i> Create a new form field </h1>
</div>
@endsection

@section('content')
@include('error')

<div class="row">
  <div class="col-md-12">
    <form action="{{ route('lead_form_fields.store') }}" method="POST">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group @if($errors->has('field_label')) has-error @endif">
        <label for="field_label-field">Field Label</label>
        <input type="text" id="field_label-field" name="field_label" class="form-control" value="{{ old("field_label") }}"/>
        @if($errors->has("field_label"))
        <span class="help-block">{{ $errors->first("field_label") }}</span>
        @endif
      </div>
      <div class="form-group @if($errors->has('weight')) has-error @endif">
        <label for="weight-field">Field Weight</label>
        {{ Form::select('weight', weightRange(), old("weight"), ['class' => 'form-control']) }}
        {{-- <input type="text" id="weight-field" name="weight" class="form-control" value="{{ old("weight") }}"/> --}}
        @if($errors->has("weight"))
        <span class="help-block">{{ $errors->first("weight") }}</span>
        @endif
      </div>
      <div class="form-group @if($errors->has('settings')) has-error @endif">
        <label for="settings-field">Field Type</label>
        <select id="field_type" class="form-control" name="field_type" required>
          <option>Please select a type</option>
          @foreach ($fieldTypes as $typeId => $type)
          <option value="{{ $typeId }}">{{ $type }}</option>
          @endforeach
        </select>
      </div>
      @include('lead_form_fields.settings')
      <textarea class="hidden form-control" id="settings-field-value" name="settings">{{ old("settings") }}</textarea>
      <div class="well well-sm">
        <button type="submit" class="btn btn-primary">Create</button>
        <a class="btn btn-link pull-right" href="{{ route('lead_form_fields.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
      </div>
    </form>
  </div>
</div>
@endsection
@section('scripts')
@section('scripts')
<script src="/js/fieldSettings.js"></script>
@endsection
@endsection
