@extends('spark::layouts.app-inner')
@section('header')
<div class="page-header">
  <h3><i class="glyphicon glyphicon-edit"></i> Edit Form Field  - {{ $lead_form_field->field_label }}</h3>
</div>
@endsection

@section('content')
@include('error')

<div class="row">
  <div class="col-md-12">
    <form action="{{ route('lead_form_fields.update', $lead_form_field->id) }}" method="POST">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group @if($errors->has('field_label')) has-error @endif">
        <label for="field_label-field">Field Label</label>
        <input type="text" id="field_label-field" name="field_label" class="form-control" value="{{ is_null(old("field_label")) ? $lead_form_field->field_label : old("field_label") }}"/>
        @if($errors->has("field_label"))
        <span class="help-block">{{ $errors->first("field_label") }}</span>
        @endif
      </div>
      <div class="form-group @if($errors->has('weight')) has-error @endif">
      <label for="weight-field">Field Weight</label>
        {{ Form::select('weight', weightRange(), is_null(old("weight")) ? $lead_form_field->weight : old("weight"), ['class' => 'form-control']) }}
        {{-- <input type="text" id="weight-field" name="weight" class="form-control" value="{{ is_null(old("weight")) ? $lead_form_field->weight : old("weight") }}"/> --}}
        @if($errors->has("weight"))
        <span class="help-block">{{ $errors->first("weight") }}</span>
        @endif
      </div>
      <div class="form-group @if($errors->has('settings')) has-error @endif">
        <label for="settings-field">Field Type</label>
        <select class="form-control" name="field_type" id="field_type" required>
          <option value="">Select a field type</option>
          @foreach ($fieldTypes as $typeId => $type)
          <option value="{{ $typeId }}" {{ $typeId == $lead_form_field->field_type ? 'selected="selected"' : ''}}>{{ $type }}</option>
          @endforeach
        </select>
      </div>
      @include('lead_form_fields.settings')
      <textarea id="settings-field-value" class="hidden form-control" rows=10 name="settings">{{ is_null(old("settings")) ? $lead_form_field->settings : old("settings") }}</textarea>
      <div class="well well-sm">
        <button type="submit" class="btn btn-primary">Save</button>
        <a class="btn btn-link pull-right" href="{{ route('lead_form_fields.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
      </div>
    </form>
  </div>
</div>
@endsection
@section('scripts')
<script src="/js/fieldSettings.js"></script>
@endsection
