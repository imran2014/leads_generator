<div class="panel panel-default" id="settings-panel">
<div class="panel-heading">Field Settings</div>
  <div class="panel-body">
    @foreach ($fieldTypes as $id => $type)
    <fieldset id="{{ $id }}-field" style="display:none;">
      {{-- <h4>Settings for {{ $type }}</h4> --}}
      @include ('lead_form_fields.fieldTypeSettings.' . $id)
    </fieldset>
    @endforeach
  </div>
</div>
