@extends('spark::layouts.app-inner')
@section('header')
<div class="page-header">
        <h1>LeadFormFields / Show #{{$lead_form_field->id}}</h1>
        <form action="{{ route('lead_form_fields.destroy', $lead_form_field->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="btn-group pull-right" role="group" aria-label="...">
                <a class="btn btn-warning btn-group" role="group" href="{{ route('lead_form_fields.edit', $lead_form_field->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
            </div>
        </form>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">

            <form action="#">
                <div class="form-group">
                    <label for="nome">ID</label>
                    <p class="form-control-static"></p>
                </div>
                <div class="form-group">
                     <label for="fieldname">FIELDNAME</label>
                     <p class="form-control-static">{{$lead_form_field->fieldname}}</p>
                </div>
                    <div class="form-group">
                     <label for="settings">SETTINGS</label>
                     <p class="form-control-static">{{$lead_form_field->settings}}</p>
                </div>
            </form>

            <a class="btn btn-link" href="{{ route('lead_form_fields.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>

        </div>
    </div>

@endsection
