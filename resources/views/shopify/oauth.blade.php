<script type='text/javascript'>
  // If the current window is the 'parent', change the URL by setting location.href
  if (window.top == window.self) {
    window.top.location.href = "/shopify/start_auth?shop={{$shop_name}}";

  // If the current window is the 'child', change the parent's URL with postMessage
  } else {
    message = JSON.stringify({
      message: "Shopify.API.remoteRedirect",
      data: { location: window.location.origin + "/shopify/start_auth?shop={{$shop_name}}" }
    });
    window.parent.postMessage(message, "https://{{$shop_name}}");
  }
</script>
