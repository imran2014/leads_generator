@extends('layouts.shopify')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-warning">
                <div class="panel-heading">Welcome Shopify User</div>

                <div class="panel-body">
                    Hello :)
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
