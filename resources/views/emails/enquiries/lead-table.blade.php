<table class="table">
  <tbody>
    <td><strong>Email: </strong></td>
    <td>{{ $lead->email }} </td>
    @foreach($lead->fields() as $field)
    <tr>
      <td><strong>{{ $field->field_label }}{{ ctype_alpha(substr($field->field_label, -1)) ? ':' : ''}} </strong></td>
      <td>
        <p>
          @if(is_array($submission->value($field->id)))
          @foreach($submission->value($field->id) as $list_item)
          {{ $list_item }}<br>
          @endforeach
          @else
          {{ $submission->value($field->id) }}
          @endif
        </p>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>
