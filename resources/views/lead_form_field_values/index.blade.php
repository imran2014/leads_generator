@extends('layout')

@section('header')
<div class="page-header clearfix">
  <h1>
    <i class="glyphicon glyphicon-align-justify"></i> Field Values
    <a class="btn btn-success pull-right" href="{{ route('lead_form_field_values.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
  </h1>

</div>
@endsection

@section('content')
<div class="row">
  <div class="col-md-12">
    @if($lead_form_field_values->count())
    <table class="table table-condensed table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>VALUE</th>
          <th class="text-right">OPTIONS</th>
        </tr>
      </thead>

      <tbody>
        @foreach($lead_form_field_values as $lead_form_field_value)
        <tr>
          <td>{{$lead_form_field_value->id}}</td>
          <td>{{$lead_form_field_value->form->name}}</td>
          <td>{{$lead_form_field_value->field->fieldname}}</td>
          <td>{{$lead_form_field_value->value}}</td>
          <td class="text-right">
            <a class="btn btn-xs btn-primary" href="{{ route('lead_form_field_values.show', $lead_form_field_value->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
            <a class="btn btn-xs btn-warning" href="{{ route('lead_form_field_values.edit', $lead_form_field_value->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            <form action="{{ route('lead_form_field_values.destroy', $lead_form_field_value->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $lead_form_field_values->render() !!}
    @else
    <h3 class="text-center alert alert-info">There are no submissions yet.</h3>
    @endif
  </div>
</div>

@endsection
