<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <h4 class="modal-title">Form HTML</h4>
</div>
<div class="modal-body">
<textarea class="form-control" rows=20>@foreach ($html as $element){{ $element . PHP_EOL }}@endforeach</textarea>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>
