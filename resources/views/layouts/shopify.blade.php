<!DOCTYPE html>
<html lang="en">
<head>
  <!-- Meta Information -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>@yield('title', 'Shopify')</title>

  <!-- Fonts -->
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600' rel='stylesheet' type='text/css'>
  <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css' rel='stylesheet' type='text/css'>

  <!-- CSS -->
  <link href="/css/sweetalert.css" rel="stylesheet">
  <link href="/css/app.css" rel="stylesheet">
  @yield('css', '')

  <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
  <script type="text/javascript">
    ShopifyApp.init({
      apiKey: '6ceb88010dd3c023813c63be8fdddff8',
      shopOrigin: 'https://{{$shop_name}}'
    });

    ShopifyApp.ready(function(){
      ShopifyApp.Bar.initialize({
        icon: '{{ url('/img/color-logo.png') }}',
        title: 'Awesome Form Builder',
        buttons: {
          primary: {
            label: 'Save',
            message: 'save',
            callback: function(){
              ShopifyApp.Bar.loadingOn();
              // doSomeCustomAction();
            }
          }
        }
      });
    });
  </script>
</head>
</head>
<body>
hello! :)
</body>
</html>

{{--
<body class="with-navbar">

  <div id="spark-app" v-cloak>
    <!-- Navigation -->
    @if (Auth::check())
    @include('spark::nav.user')
    @else
    @include('spark::nav.guest')
    @endif

    <!-- Main Content -->
    <div class="container">
      @yield('header')
      @yield('content')
    </div>

    <!-- Application Level Modals -->
    @if (Auth::check())
    @include('spark::modals.notifications')
    @include('spark::modals.support')
    @include('spark::modals.session-expired')
    @endif
  </div>

  <!-- JavaScript -->
  <script src="/js/app.js"></script>
  <script src="/js/sweetalert.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  @yield('scripts', '')
</body>

--}}
