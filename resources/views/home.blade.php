@extends('spark::layouts.app')

@section('content')
<home :user="user" inline-template>
  <div class="container">
    <!-- Application Dashboard -->
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">AutoCoda Leads Generator</div>
          <div class="panel-body">
            Welcome to AutoCoda Leads Generator where we keep all leads in one convenient place!
          </div>
        </div>
      </div>
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Leads</div>
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>Email</th>
                  <th>Form</th>
                  {{-- <th>Enquiry</th> --}}
                  <th>Submission Date</th>
                </tr>
              </thead>
              <tbody>
                @if(count($leads))
                @foreach ($leads as $lead)
                <tr>
                  <td><a href="{{ $lead->path() }}">{{ $lead->email }}</a></td>
                  <td>{{ $lead->form()->title }}</td>
                  {{-- <td><a href="{{ $lead->path() }}">{{ $lead->enquiry_short }}</a></td> --}}
                  <td>{{ $lead->display_date }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                <td colspan=4 class="text-center">There are no leads yet.</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
          <div class="panel-heading">Lead Forms</div>
          <div class="panel-body">
            <table class="table">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Description</th>
                  <th>Last Updated</th>
                </tr>
              </thead>
              <tbody>
                @if(count($leadforms))
                @foreach ($leadforms as $form)
                <tr>
                  <td><a href="{{ $form->path() }}">{{ $form->title }}</a></td>
                  <td>{{ $form->description }}</td>
                  <td>{{ $form->display_date }}</td>
                </tr>
                @endforeach
                @else
                <tr>
                  <td colspan=3 class="text-center">You have no forms. <a href="{{ url('lead_forms/create')}}">Make one here</a>.</td>
                </tr>
                @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</home>
@endsection
