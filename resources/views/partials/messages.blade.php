<div class="row">
  <div class="col-sm-12">
    @if (session('message'))
    <div class="alert alert-success">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('message') }}
    </div>
    @endif

    @if (session('error'))
    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> {{ session('error') }}
    </div>
    @endif
  </div>
</div>
