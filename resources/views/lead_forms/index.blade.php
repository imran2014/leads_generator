@extends('spark::layouts.app-inner')
@section('header')
<div class="page-header clearfix">
  <h1>
    <i class="glyphicon glyphicon-align-justify"></i> Forms
    <a class="btn btn-success pull-right" href="{{ route('lead_forms.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a>
  </h1>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    @if($lead_forms->count())
    <table class="table table-condensed table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Title</th>
          <th>Description</th>
          {{-- <th>URLs</th> --}}
          <th>Redirect</th>
          <th>Main Email</th>
          <th class="text-right">OPTIONS</th>
        </tr>
      </thead>
      <tbody>
        @foreach($lead_forms as $lead_form)
        <tr>
          <td>{{$lead_form->id}}</td>
          <td>{{$lead_form->title}}</td>
          <td>{{$lead_form->description}}</td>
          <td>{{$lead_form->redirect}}</td>
          <td>{{$lead_form->email_to}}</td>
          {{-- <td>{{$lead_form->displayable_on}}</td> --}}
          <td class="text-right">
            {{-- <a class="btn btn-xs btn-primary" href="{{ route('lead_forms.show', $lead_form->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a> --}}
            <a class="btn btn-xs btn-warning" href="{{ route('lead_forms.edit', $lead_form->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            <a class="btn btn-xs btn-info" href="{{ url('render/form', $lead_form->id)}}" data-toggle="modal" data-target="#htmlRender"><i class="glyphicon glyphicon-edit"></i> Generate HTML</a>
            <form action="{{ route('lead_forms.destroy', $lead_form->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button>
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $lead_forms->render() !!}
    @else
    <h3 class="text-center alert alert-info">There are no lead forms yet.</h3>
    @endif
  </div>
</div>

<div class="modal fade" id="htmlRender" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content"></div>
  </div>
</div>
@endsection
@section('scripts')
<script>
  (function($){
    $('.modal').on('hide.bs.modal', function (e) {
      var modalData = $(this).data('bs.modal');
      // Destroy modal if has remote source – don't want to destroy modals with static content.
      if (modalData && modalData.options.remote) {
        // Destroy component. Next time new component is created and loads fresh content
        $(this).removeData('bs.modal');
        // Also clear loaded content, otherwise it would flash before new one is loaded.
        $(this).find(".modal-content").empty();
      }
    });
  })(jQuery)
</script>
@endsection
