@extends('spark::layouts.app-inner')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
<div class="page-header">
  <h1><i class="glyphicon glyphicon-plus"></i> Create a new Lead Form</h1>
</div>
@endsection

@section('content')
@include('error')

<div class="row">
  <div class="col-md-12">
    @if(!count($fields))
    <div class="panel panel-warning">
      <div class="panel-heading">No form fields available.</div>
      <div class="panel-body">
       You cannot save a form without any fields.
       <a href="{{ url('lead_form_fields/create') }}">Create some here.</a>
     </div>
   </div>
   <span> </span>
   @endif

   <form action="{{ route('lead_forms.store') }}" method="POST">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">

    <div class="form-group @if($errors->has('title')) has-error @endif">
     <label for="title-field">Title</label>
     <input type="text" id="title-field" name="title" class="form-control" value="{{ old("title") }}" required>
     @if($errors->has("title"))
     <span class="help-block">{{ $errors->first("title") }}</span>
     @endif
   </div>
   <div class="form-group @if($errors->has('description')) has-error @endif">
     <label for="description-field">Description</label>
     <textarea class="form-control" id="description-field" rows="3" name="description">{{ old("description") }}</textarea>
     @if($errors->has("description"))
     <span class="help-block">{{ $errors->first("description") }}</span>
     @endif
   </div>
   <div class="form-group @if($errors->has('redirect')) has-error @endif">
     <label for="redirect-field">Redirection Location</label>
     <input type="text" id="redirect-field" name="redirect" class="form-control" value="{{ old("redirect") }}" required>
     <span class="help-block">Where will this form redirect once submitted?</span>
     @if($errors->has("redirect"))
     <span class="help-block">{{ $errors->first("redirect") }}</span>
     @endif
   </div>
   <div class="form-group @if($errors->has('email_to')) has-error @endif">
     <label for="email_to-field">Email the enquires to who?</label>
     <input type="text" id="email_to-field" name="email_to" class="form-control" value="{{ old("email_to") }}"/>
     <span class="help-block">Who will this email sent to? Comma separated for multiple emails.</span>
     @if($errors->has("email_to"))
     <span class="help-block">{{ $errors->first("email_to") }}</span>
     @endif
   </div>
   <div class="form-group @if($errors->has('email_cc')) has-error @endif">
     <label for="email_cc-field">CC emails for enquires</label>
     <input type="text" id="email_cc-field" name="email_cc" class="form-control" value="{{ old("email_cc") }}"/>
     <span class="help-block">Who will this email cc to? Comma separated for multiple emails.</span>
     @if($errors->has("email_cc"))
     <span class="help-block">{{ $errors->first("email_cc") }}</span>
     @endif
   </div>
   <div class="form-group @if($errors->has('email_bcc')) has-error @endif">
     <label for="email_bcc-field">BCC for enquiries</label>
     <input type="text" id="email_bcc-field" name="email_bcc" class="form-control" value="{{ old("email_bcc") }}"/>
     <span class="help-block">Who will this email bcc to? Comma separated for multiple emails.</span>
     @if($errors->has("email_bcc"))
     <span class="help-block">{{ $errors->first("email_bcc") }}</span>
     @endif
   </div>
{{--  <div class="form-group @if($errors->has('displayable_on')) has-error @endif">
       <label for="displayable_on-field">Urls Displayable On</label>
       <textarea class="form-control" id="displayable_on-field" rows="3" name="displayable_on">{{ old("displayable_on") }}</textarea>
       <span class="help-block">On what URLs can this form be displayed? Can use wildcards e.g: mywebsite.com/* or a specific url e.g: mywebsite.com/example</span>
       @if($errors->has("displayable_on"))
       <span class="help-block">{{ $errors->first("displayable_on") }}</span>
       @endif
     </div> --}}
   <div class="form-group @if($errors->has('fields')) has-error @endif">
     <label for="fields-field">Form Fields</label>
     @foreach($fields as $field)
     <div class="checkbox">
      <label>
        <input type="checkbox" name="fields[]" value="{{ $field->id }}" {{ $field->field_type == 'submit' ? 'checked=checked' : ''}}> {{ $field->field_label }}
      </label>
    </div>
    @endforeach
    <span class="help-block">Select which fields should display on this form.</span>
    @if($errors->has("fields"))
    <span class="help-block">{{ $errors->first("fields") }}</span>
    @endif
  </div>
  <div class="well well-sm">
    @if(count($fields))
    <button type="submit" class="btn btn-primary">Create</button>
    @endif
    <a class="btn btn-link pull-right" href="{{ route('lead_forms.index') }}"><i class="glyphicon glyphicon-backward"></i> Back</a>
  </div>
</form>

</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
<script>
  $('.date-picker').datepicker({
  });
</script>
@endsection
