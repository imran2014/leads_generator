@extends('spark::layouts.app-inner')
@section('header')
<div class="page-header clearfix">
  <h1>
    <i class="glyphicon glyphicon-align-justify"></i> Leads
    {{-- <a class="btn btn-success pull-right" href="{{ route('leads.create') }}"><i class="glyphicon glyphicon-plus"></i> Create</a> --}}
  </h1>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    @if($leads->count())
    <table class="table table-condensed table-striped">
      <thead>
        <tr>
          <th>ID</th>
          <th>Email</th>
          <th>Form</th>
          <th># Submissions</th>
          <th>Progression</th>
          {{-- <th>Url</th> --}}
          <th class="text-right">OPTIONS</th>
        </tr>
      </thead>
      <tbody>
        @foreach($leads as $lead)
        <tr>
          <td>{{ $lead->id }}</td>
          <td>{{ $lead->email }}</td>
          <td>{{ $lead->form()->title }}</td>
          <td>{{ count($lead->submissions) }}</td>
          <td>TODO</td>
          {{-- <td>TODO</td> --}}
          <td class="text-right">
            <a class="btn btn-xs btn-primary" href="{{ route('leads.show', $lead->id) }}"><i class="glyphicon glyphicon-eye-open"></i> View</a>
            <a class="btn btn-xs btn-warning" href="{{ route('leads.edit', $lead->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            <form action="{{ route('leads.destroy', $lead->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
              <input type="hidden" name="_method" value="DELETE">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              {{-- <button type="submit" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete</button> --}}
            </form>
          </td>
        </tr>
        @endforeach
      </tbody>
    </table>
    {!! $leads->render() !!}
    @else
    <h3 class="text-center alert alert-info">There are no leads yet.</h3>
    @endif
  </div>
</div>
@endsection
