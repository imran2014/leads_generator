@extends('spark::layouts.app-inner')
@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/css/bootstrap-datepicker.css" rel="stylesheet">
@endsection
@section('header')
<div class="page-header">
  <h1><i class="glyphicon glyphicon-edit"></i> Edit Lead #{{$lead->id}}</h1>
</div>
@endsection

@section('content')
@include('error')

<div class="row">
  <div class="col-md-12">
    <form action="{{ route('leads.update', $lead->id) }}" method="POST">
      <input type="hidden" name="_method" value="PUT">
      <input type="hidden" name="_token" value="{{ csrf_token() }}">
      <div class="form-group @if($errors->has('email')) has-error @endif">
       <label for="name-field">Email</label>
       <input type="email" id="name-field" name="name" class="form-control" value="{{ is_null(old("email")) ? $lead->email : old("email") }}"/>
       @if($errors->has("email"))
       <span class="help-block">{{ $errors->first("email") }}</span>
       @endif
     </div>
     <div class="well well-sm">
      <button type="submit" class="btn btn-primary">Save</button>
      <a class="btn btn-link pull-right" href="{{ route('leads.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
    </div>
  </form>

</div>
</div>
@endsection
@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.0/js/bootstrap-datepicker.min.js"></script>
<script>
  $('.date-picker').datepicker({
  });
</script>
@endsection
