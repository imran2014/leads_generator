<table class="table">
  <thead>
    <tr>
      @foreach($lead->fields() as $field)
      <th>{{ $field->field_label }}</th>
      @endforeach
      <th>Submitted</th>
    </tr>
  </thead>
  <tbody>
    @foreach($lead->teamSubmissions() as $submission)
    <tr>
      @foreach($lead->fields() as $field)
      <td>
        @if(is_array($submission->value($field->id)))
        <ul>
          @foreach($submission->value($field->id) as $list_item)
          <li>{{ $list_item }}</li>
          @endforeach
        </ul>
        @else
        {{ $submission->value($field->id) }}
        @endif
      </td>
      @endforeach
      <td>{{ $submission->created_at->diffForHumans() }}</td>
    </tr>
    @endforeach
  </tbody>
</table>
