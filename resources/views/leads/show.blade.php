@extends('spark::layouts.app-inner')
@section('header')
<div class="page-header">
  <h1>Lead #{{$lead->id}}</h1>
  <form action="{{ route('leads.destroy', $lead->id) }}" method="POST" style="display: inline;" onsubmit="if(confirm('Delete? Are you sure?')) { return true } else {return false };">
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="btn-group pull-right" role="group" aria-label="...">
      <a class="btn btn-warning btn-group" role="group" href="{{ route('leads.edit', $lead->id) }}"><i class="glyphicon glyphicon-edit"></i> Edit</a>
      <button type="submit" class="btn btn-danger">Delete <i class="glyphicon glyphicon-trash"></i></button>
    </div>
  </form>
</div>
@endsection
@section('content')
<div class="row">
  <div class="col-md-12">
    <form action="#">
      <div class="form-group">
        <label for="nome">ID</label>
        <p class="form-control-static">{{$lead->id}}</p>
      </div>
      <div class="form-group">
        <label for="name">Email</label>
        <p class="form-control-static">{{$lead->email}}</p>
      </div>
      <h3>Submissions</h3>
      @include ('leads.lead-table')
{{--       <div class="form-group">
        <label for="subject">SUBJECT</label>
        <p class="form-control-static">{{$lead->subject}}</p>
      </div>
      <div class="form-group">
        <label for="enquiry">ENQUIRY</label>
        <p class="form-control-static">{{$lead->enquiry}}</p>
      </div> --}}
    </form>
    <a class="btn btn-link" href="{{ route('leads.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
  </div>
</div>

@endsection
