<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('submissions', function(Blueprint $table) {
        $table->increments('id');
        $table->integer('team_id')->unsigned()->index();
        $table->integer('lead_id')->unsigned()->index();
        $table->integer('lead_form_id')->unsigned()->index();
        $table->timestamps();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::drop('submissions');
    }
  }
