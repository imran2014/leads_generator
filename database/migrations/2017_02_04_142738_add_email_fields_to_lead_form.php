<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEmailFieldsToLeadForm extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
            $table->string('email_to');
            $table->string('email_cc');
            $table->string('email_bcc');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_forms', function (Blueprint $table) {
           $table->dropColumn('email_to');
           $table->dropColumn('email_cc');
           $table->dropColumn('email_bcc');
       });
    }
}
