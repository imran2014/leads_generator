<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadFormsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('lead_forms', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('team_id')->unsigned()->index();
      $table->string('title');
      $table->text('redirect');
      $table->text('description');
      $table->text('displayable_on');
      $table->timestamps();
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lead_forms');
	}

}
