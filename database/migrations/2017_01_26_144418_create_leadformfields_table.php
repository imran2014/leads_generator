<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadFormFieldsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('lead_form_fields', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('team_id')->unsigned()->index();
      $table->integer('weight');
      $table->string('field_label');
      $table->string('field_type');
      $table->string('machine_name');
      $table->longText('settings');
      $table->timestamps();
    });

    //Create the table for the many to many relationship that is between
    //Forms and fields
    Schema::create('lead_form_lead_form_fields', function (Blueprint $table) {
      $table->integer('form_id');
      $table->integer('form_field_id');
      $table->integer('weight');

      $table->unique(['form_id', 'form_field_id']);
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop('lead_form_fields');
  }

}
