<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('leads', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('team_id')->unsigned()->index();
      $table->integer('lead_form_id')->unsigned()->index();
      $table->integer('lead_status')->unsigned()->index();
      $table->string('email');
      $table->timestamps();
    });
  }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
  public function down()
  {
    Schema::drop('leads');
  }

}
