<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldValuesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
    Schema::create('field_values', function(Blueprint $table) {
      $table->increments('id');
      $table->integer('submission_id')->unsigned()->index();
      $table->integer('lead_form_field_id')->unsigned()->index();
      $table->longText('value');
    });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
  {
    Schema::drop('field_values');
  }

}
