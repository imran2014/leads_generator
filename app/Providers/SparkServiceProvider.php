<?php

namespace App\Providers;

use Laravel\Spark\Spark;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'AutoCoda',
        'product' => 'AutoCoda Leads',
        'street' => 'Ground Floor, 19 Riseley St',
        'location' => 'Ardross WA 6153',
        'phone' => '0422 036 008',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = "team@autocoda.com";

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        "i.bajrai@gmail.com"
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = true;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        Spark::useRoles([
            'leader' => 'Leader',
            'member' => 'Member',
        ]);
        Spark::useStripe()->noCardUpFront()->trialDays(30);

        Spark::freePlan()
            ->features([
                'Basic account to use and test the system.'
            ])
            ->maxTeams(1);

        Spark::plan('Basic', 'provider-id-1')
            ->price(20)
            ->features([
                'Required to continue using the system after the trial period.'
            ])
            ->maxTeams(1);
    }
}
