<?php

namespace App\Providers;

use Laravel\Spark\Providers\SparkServiceProvider;

class OverrideSparkDefaultServiceProvider extends SparkServiceProvider
{

    protected function registerServices()
    {
      parent::registerServices();

        $services = [
            'Contracts\Interactions\Settings\Teams\CreateTeam' => 'Teams\AutocodaCreateTeam',
        ];

        foreach ($services as $key => $value) {
            $this->app->singleton('Laravel\Spark\\'.$key, 'App\Autocoda\\'.$value);
        }
    }
}
