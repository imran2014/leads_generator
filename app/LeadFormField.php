<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Repositories\FieldTypes;

class LeadFormField extends Model
{
  use HasTeamScope;

  public function scopeTeam($query, $user = NULL, $team_id = NULL)
  {
    if(is_null($team_id)){
      $team_id = $this->team_id;
    }

    if(is_null($user)){
      $user = Auth::user();
    }

    if($user && is_null($team_id)){
      $team_id = $user->current_team->id;
    }

    return $query->where('team_id', $team_id)->orderBy('weight');
  }

  public function form()
  {
    return $this->belongsToMany(LeadForm::class, 'lead_form_lead_form_fields', 'form_field_id', 'form_id');
  }

  public function values()
  {
    return $this->hasMany(FieldValue::class);
  }

  public function save(array $options = [])
  {
    if( ! $this->team_id){
      $this->team_id = Auth::user()->currentTeam->id;
    }

    if($this->settings){
      $this->settings = json_encode($this->settings);
    }

    if( ! $this->machine_name){
      $this->machine_name = strtolower(strtr(preg_replace("/[^A-Za-z0-9 ]/", '', trim($this->field_label)), ' ', '_'));
    }

    if( ! $this->weight){
      $this->weight = -50;
    }

    parent::save($options);
  }

  public function getFieldTypeHumanAttribute($value)
  {
    return app(FieldTypes::class)->getType($this->field_type);
  }

  public function buildHTML()
  {
    dd('Not built yet');
  }

  public function getSettingsAttribute($value)
  {
    if(is_object(json_decode($value))){
      return $value;
    }

    return json_decode($value);
  }

  public function getSettingsObjectAttribute($value)
  {
    return json_decode($this->settings);
  }

  public function getSettingsArrayAttribute($value)
  {
    return json_decode($this->settings, TRUE);
  }

  public function setSettingsAttribute($value)
  {
    $this->attributes['settings'] = json_encode(json_decode($value, true));
  }
}
