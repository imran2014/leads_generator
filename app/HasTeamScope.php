<?php

namespace App;
use Auth;

trait HasTeamScope
{
  public function scopeTeam($query, $user = NULL, $team_id = NULL)
  {
    if(is_null($team_id)){
      $team_id = $this->team_id;
    }

    if(is_null($user)){
      $user = Auth::user();
    }

    if($user && is_null($team_id)){
      $team_id = $user->current_team->id;
    }

    return $query->where('team_id', $team_id);
  }

  public function save(array $options = [])
  {
    if( ! $this->team_id)
    {
      if(Auth::user()->currentTeam){
        $this->team_id = Auth::user()->currentTeam->id;
      }else{
        throw new Exception('You did not assign a team id.');
      }
    }

    parent::save($options);
  }

  public function owningTeam(){
    return $this->belongsTo(Team::class);
  }
}
