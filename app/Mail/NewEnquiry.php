<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Lead;
use App\Submission;

class NewEnquiry extends Mailable
{
  use Queueable, SerializesModels;

  public $lead;
  public $submission;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(Lead $lead, Submission $submission)
  {
    $this->lead = $lead;
    $this->submission = $submission;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this
    ->from($this->lead->email, env('MAIL_FROM_NAME', 'AutoLeads'))
    ->subject($this->newLeadSubject())
    ->view('emails.enquiries.new');
  }

  protected function newLeadSubject(){
    return "You have recieved a new lead: {$this->lead->email} from {$this->lead->form()->title}";
  }
}
