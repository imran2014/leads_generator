<?php

namespace App\Shopify;

use  App\Shopify\Auth\ShopifyOAuth;
use Exception;

class ShopifyClient{

  protected static $instance;
  protected $domain;
  protected $api_key = '6ceb88010dd3c023813c63be8fdddff8';
  protected $secret = '79bf3ba028e7e17ab6a56e942774ceb0';

  protected function __construct($domain){
    $this->domain = $domain;
    $this->oAuth = new ShopifyOAuth($this->domain, '', $this->api_key, $this->secret);
  }

  public static function create($domain){
    if(!self::$instance){
      static::$instance = new self($domain);
    }

    return static::$instance;
  }

  public static function createOAuth($domain){
    $instance = static::create($domain);

    return $instance->oAuth;
  }

  public function getAuthorizeUrl($nonce){
    $scope = 'read_content,write_content';
    $url = url('shopify/authorise');
    //Testing redirect, don't use this normally.
    // $url = 'http://ab9e0c36.ngrok.io/shopify/authorise';

    return $this->oAuth->getAuthorizeUrl($scope, $url) . '&state=' . $nonce;
  }


  public function getAccessToken($code){
    return $this->oAuth->getAccessToken($code);
  }

  protected function installAppUrl(){
    // https://autoleads.myshopify.com/admin/oauth/authorize?client_id=6ceb88010dd3c023813c63be8fdddff8&scope=read_content,write_content&redirect_uri=http://leads.autocoda.com/shopify/register
    // https://autoleads.myshopify.com/admin/oauth/authorize?client_id=6ceb88010dd3c023813c63be8fdddff8&scope=read_content,write_content&redirect_uri=http://leads.autocoda.com/shopify&state={nonce}&grant_options[]={option}
  }

  protected function scopes(){
    // read_content, write_content
    // read_themes, write_themes
    // read_products, write_products
    // read_customers, write_customers
    // read_orders, write_orders
    // read_script_tags, write_script_tags
    // read_fulfillments, write_fulfillments
    // read_shipping, write_shipping
    // read_analytics
    // read_users, write_users
  }
}

