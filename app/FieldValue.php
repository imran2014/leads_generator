<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class FieldValue extends Model
{

  public $timestamps = false;

  public static function createNewFieldValue(LeadFormField $field, Submission $submission, $value){
    $fieldValue = new self;
    $fieldValue->lead_form_field_id = $field->id;
    $fieldValue->submission_id = $submission->id;
    $fieldValue->value = $value;
    $fieldValue->save();

    return $fieldValue;
  }

  public function form()
  {
    return $this->submission()->form;
  }

  public function leadForm()
  {
    return $this->belongsTo(LeadForm::class);
  }

  public function field()
  {
    return $this->belongsTo(LeadFormField::class);
  }

  public function submission()
  {
    return $this->belongsTo(Submission::class);
  }

  public function lead()
  {
    return $this->submission()->lead;
  }

  public function setValueAttribute($value)
  {
    if(is_array($value)){
      $value = json_encode($value);
    }
    $this->attributes['value'] = $value;
  }

  public function getValueAttribute($value)
  {
    $object = json_decode($value);
    if(is_array($object)){
      return $object;
    }

    return $value;
  }

}
