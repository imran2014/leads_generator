<?php

namespace App\Autocoda\Teams;

use Laravel\Spark\Interactions\Settings\Teams\CreateTeam;
use Spark;

class AutocodaCreateTeam extends CreateTeam
{
  protected function validateMaximumTeamsNotExceeded($validator, $user)
  {
    if (! $plan = $user->sparkPlan()) {
      return;
    }

    if (is_null($plan->teams)) {
      return;
    }
    if ($plan->teams <= $user->ownedTeams()->count()) {
      $validator->errors()->add('name', 'No additional '.str_plural(Spark::teamString()).' can be created.');
    }
  }
}
