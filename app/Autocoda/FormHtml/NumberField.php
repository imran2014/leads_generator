<?php

namespace App\Autocoda\FormHtml;

class NumberField implements FormFieldContract{
  use FormFieldTrait;

  protected $formFieldType = 'number';

}
