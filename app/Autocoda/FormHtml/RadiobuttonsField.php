<?php

namespace App\Autocoda\FormHtml;

use Illuminate\Support\HtmlString;
use App\LeadFormField;
use Form;
class RadiobuttonsField implements FormFieldContract{
  use FormFieldTrait{
    render as traitRender;
  }

  protected $formFieldType = 'radio';

  public function render(LeadFormField $field){
    list($label, $html_field) = $this->traitRender($field);
    $values = $field->settings_object;
    if(!empty($values->values)){
      $html = '';
      foreach(explode(PHP_EOL, $values->values) as $key => $value){
        $options = $this->buildFieldOptions($field);
        unset($options['required']);
        $html .= '<label>' . Form::{$this->formFieldType}($field->machine_name . '[]',  $value, NULL, $options)->toHtml() . trim($value) .  '</label>' . PHP_EOL;
      }
      $html_field = new HtmlString(rtrim($html));
    }

    return [ $label, $html_field ];
  }
}
