<?php

namespace App\Autocoda\FormHtml;

use App\LeadFormField;
use Form;

interface FormFieldContract{
  public function render(LeadFormField $field);
}
