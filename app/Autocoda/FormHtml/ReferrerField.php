<?php

namespace App\Autocoda\FormHtml;

use Illuminate\Support\HtmlString;
use App\LeadFormField;

class ReferrerField implements FormFieldContract{

  public function render(LeadFormField $field){
    return [new HtmlString(''), new HtmlString('')];
  }

}
