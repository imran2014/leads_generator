<?php

namespace App\Autocoda\FormHtml;

class TextField implements FormFieldContract{
  use FormFieldTrait;

  protected $formFieldType = 'text';

}
