<?php

namespace App\Autocoda\FormHtml;

class EmailField implements FormFieldContract{
  use FormFieldTrait;

  protected $formFieldType = 'email';
}
