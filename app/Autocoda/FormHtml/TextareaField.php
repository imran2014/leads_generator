<?php

namespace App\Autocoda\FormHtml;

class TextareaField implements FormFieldContract{
  use FormFieldTrait;

  protected $formFieldType = 'textarea';
}
