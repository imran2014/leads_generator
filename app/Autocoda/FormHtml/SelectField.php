<?php

namespace App\Autocoda\FormHtml;

use Illuminate\Support\HtmlString;
use App\LeadFormField;
use Form;

class SelectField implements FormFieldContract{
  use FormFieldTrait{
    render as traitRender;
  }

  protected $formFieldType = 'select';

  public function render(LeadFormField $field){
    $settings = $field->settings_object;
    $options = $this->buildFieldOptions($field);

    if(!empty($settings->values)){
      $values = explode(PHP_EOL, $settings->values);

      if(empty($options['select-first'])){
        $options['select-first'] = 'Please select';
      }

      $values = ['' => $options['select-first']] + array_combine($values, $values);

      $label = Form::label($field->machine_name, $field->field_label);
      $field = Form::{$this->formFieldType}($field->machine_name, $values, NULL, $options);

      return [ $label, $field ];
    }
  }

}
