<?php

namespace App\Autocoda\FormHtml;

use App\LeadFormField;
use Illuminate\Support\HtmlString;
use Form;

class SubmitField implements FormFieldContract{
  use FormFieldTrait{
    render as traitRender;
  }

  protected $formFieldType = 'submit';


  public function render(LeadFormField $field){
    $options = $this->buildFieldOptions($field);
    $value = $field->field_label;

    if(!empty($options['submit-value'])){
      $value = trim($options['submit-value']);
    }

    unset($options['submit-value']);
    $options['type'] = 'submit';

    return [ new HtmlString(''), Form::button($value, $options) ];
  }
}
