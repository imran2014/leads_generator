<?php

namespace App\Autocoda\FormHtml;

use App\LeadFormField;
use Form;

trait FormFieldTrait
{
  public function render(LeadFormField $field){
    $options = $this->buildFieldOptions($field);
    $label = Form::label($field->machine_name, $field->field_label);
    $field = Form::{$this->formFieldType}($field->machine_name, NULL, $options);
    return [ $label, $field ];
  }

  /**
   * Map for fields settings saved against field to valid field options.
   */
  protected function allowedOptions(){
    return [
    'class' => 'css-class',
    'required' => 'required',
    'placeholder' => 'placeholder',
    'rows' => 'textarea-rows',
    'submit-value' => 'submit-value',
    'select-first' => 'select-first',
    ];
  }

  protected function buildFieldOptions(LeadFormField $field){
    $options = [];

    foreach($this->allowedOptions() as $option => $type){
      if(isset($field->settingsArray[$type])){
        $options[$option] = $field->settingsArray[$type];
      }
    }

    //TODO: add an option to make bootstrap styling optional.
    $this->addBootstrapFormClass($options);

    return $options;
  }

  protected function addBootstrapFormClass(&$options){
    if( ! isset($options['class'])){
      $options['class'] = '';
    }

    if(strpos($options['class'], 'form-control') === FALSE && $this->formFieldType != 'submit'){
      $options['class'] .= ' form-control';
    }
  }

}
