<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopifyStoreCredentials extends Model
{
    public $fillable = ['store', 'nonce'];
}
