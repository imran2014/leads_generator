<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Submission;
use Mail;
use App\Mail\NewEnquiry;

class SendEmailsController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function sendSubmission(Submission $submission)
  {
    $lead = $submission->lead;
    $lead->form()->mail(new NewEnquiry($lead, $submission));
    return 'Email Sent';
  }
}
