<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Lead;
use Illuminate\Http\Request;

class LeadController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $leads = Lead::team()->orderBy('id', 'desc')->paginate(10);

    return view('leads.index', compact('leads'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Responsek
   */
  public function create()
  {
    return view('leads.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $lead = new Lead();

    $lead->name = $request->input("name");
    $lead->subject = $request->input("subject");
    $lead->enquiry = $request->input("enquiry");

    $lead->save();

    return redirect()->route('leads.index')->with('message', 'Item created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lead = Lead::findOrFail($id);

    return view('leads.show', compact('lead'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $lead = Lead::findOrFail($id);

    return view('leads.edit', compact('lead'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $lead = Lead::findOrFail($id);

    $lead->name = $request->input("name");
    $lead->subject = $request->input("subject");
    $lead->enquiry = $request->input("enquiry");

    $lead->save();

    return redirect()->route('leads.index')->with('message', 'Item updated successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $lead = Lead::findOrFail($id);
    $lead->values()->delete();
    $lead->submissions()->delete();
    $lead->delete();

    return redirect()->route('leads.index')->with('message', 'Item deleted successfully.');
  }

}
