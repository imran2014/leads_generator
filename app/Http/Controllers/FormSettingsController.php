<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lead;

class FormSettingsController extends Controller
{

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show(Lead $lead)
  {
    return view('leads.show', compact('lead'));
  }
}
