<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ShopifyController extends Controller
{
  public function index(Request $request){
    $shop_name = $request->shop;
    //Create thank you page if it doesn't exist.
    //Tell the user you just created it.
    return view('shopify.home', ['shop_name' => $shop_name]);
  }
}
