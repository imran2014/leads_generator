<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Repositories\FieldTypes;
use App\LeadFormField;
use Illuminate\Http\Request;

class LeadFormFieldController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $lead_form_fields = LeadFormField::team()->orderBy('weight')->paginate(50);

    return view('lead_form_fields.index', compact('lead_form_fields'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $fieldTypes = (new FieldTypes)->get();
    return view('lead_form_fields.create', compact('fieldTypes'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $lead_form_field = new LeadFormField();

    $lead_form_field->field_label = $request->input("field_label");
    $lead_form_field->field_type = $request->input("field_type");
    $lead_form_field->settings = $request->input("settings");
    $lead_form_field->weight = $request->input("weight");

    $lead_form_field->save();

    return redirect()->route('lead_form_fields.index')->with('message', 'Item created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lead_form_field = LeadFormField::findOrFail($id);

    return view('lead_form_fields.show', compact('lead_form_field'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $lead_form_field = LeadFormField::findOrFail($id);
    $fieldTypes = (new FieldTypes)->get();

    return view('lead_form_fields.edit', compact('lead_form_field', 'fieldTypes'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $lead_form_field = LeadFormField::findOrFail($id);

    $lead_form_field->field_label = $request->input("field_label");
    $lead_form_field->field_type = $request->input("field_type");
    $lead_form_field->settings = $request->input("settings");
    $lead_form_field->weight = $request->input("weight");

    $lead_form_field->save();

    return redirect()->route('lead_form_fields.index')->with('message', 'Item updated successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $lead_form_field = LeadFormField::findOrFail($id);

    if(count($lead_form_field->form)){
      return back()->with('error', 'You cannot delete this field, it is used on a form.');
    }

    $lead_form_field->delete();

    return redirect()->route('lead_form_fields.index')->with('message', 'Item deleted successfully.');
  }

}
