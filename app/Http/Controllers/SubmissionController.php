<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\NewEnquiry;
use App\Lead;
use App\Submission;
use App\LeadForm;
use App\LeadFormField;
use App\FieldValue;
use App\Repositories\FieldTypes;
use URL;
use Validator;

class SubmissionController extends Controller
{
  protected $fieldTypeRepository;

  public function __construct(FieldTypes $fieldTypeRepository)
  {
    $this->fieldTypeRepository = $fieldTypeRepository;
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $redirect = $request->redirect ?: $form->redirect;

    //Abort the submission, this probably is a bot.
    if($request->super_required_field){
      return redirect($redirect . '?form-submitted=1');
    }

    $form = LeadForm::findOrFail($request->form_id);

    //The user did not fill out the form correctly, show them the errors.
    $errors = $this->performValidation($request, $form);
    if($errors){
      return view('submissions.form_errors', compact('form', 'errors'));
    }

    $email = $form->emailFieldInRequest($request);
    $lead = Lead::existingLead($email, $form);
    $submission = Submission::createNewSubmission($form, $lead);
    $this->createFieldSubmissions($form, $request, $submission);

    $form->mail(new NewEnquiry($lead, $submission));

    return redirect($redirect . '?form-submitted=1');
  }

  protected function createFieldSubmissions(LeadForm $form, Request $request, Submission $submission){
    $email = $form->emailFieldInRequest($request);
    foreach($form->fields()->get() as $field){
      if($request->has($field->machine_name)){
        //Make this a check if the field type is of email.
        if($email == $request->{$field->machine_name}){
          continue;
        }
        $value = $request->{$field->machine_name};
        FieldValue::createNewFieldValue($field, $submission, $value);
      }
    }
    $this->addRefererToSubmission($submission, $request);
  }

  protected function performValidation(Request $request, LeadForm $form){
    $rules = [];
    foreach($form->fields()->get() as $formField){
      if($this->isFieldRequired($formField)){
        $rules[$formField->machine_name][] = 'required';
      }
      if($this->fieldTypeRepository->isEmailFieldType($formField->field_type)){
        $rules[$formField->machine_name][] = 'email';
      }
      if(!empty($rules[$formField->machine_name])){
        $rules[$formField->machine_name] = join('|', $rules[$formField->machine_name]);
      }
    }
    $validator = Validator::make($request->all(), $rules);
    if($validator->fails()){
      return $validator->errors();
    }
  }

  protected function addRefererToSubmission(Submission $submission, Request $request){
    $refererField = LeadFormField::where('field_type', 'page_referer')->first();
    FieldValue::createNewFieldValue($refererField, $submission, $request->headers->get('referer', 'Unknown'));
  }

  protected function isFieldRequired($formField){
    $settings = $formField->settingsArray;
    $requiredSetting = isset($settings['required']) ? $settings['required'] : false;
    return $this->fieldTypeRepository->isRequiredFieldType($formField->field_type) || $requiredSetting;
  }

}
