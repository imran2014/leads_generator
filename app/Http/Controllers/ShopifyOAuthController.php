<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shopify\ShopifyClient;
use App\ShopifyStoreCredentials;
use Exception;

class ShopifyOAuthController extends Controller
{

  // See https://help.shopify.com/api/guides/authentication/oauth
  // Step #1 - User clicks the link in the shopify store.
  // this contains the shop url so you can start processing the authentication.
  // eg http://leads.autocoda.com/shopify/register?shop=MYSHOP.myshopify.com
  // Step #2 - Redirect the user to the authorisation page so that you can tell
  // them what permissions you need and they click install.
  // Step #3 - Confirming the install
  // Step #4 - Making authenticated requests
  public function register(Request $request){
    $shop_name = $request->shop;

    if(!$shop_name){
      throw new Exception('Your request does not contain the shop parameter.');
    }

    return view('shopify.oauth', compact('shop_name'));
  }

  public function startAuth(Request $request){
    $shop_name = $request->shop;

    if(!$shop_name){
      throw new Exception('Your request does not contain the shop parameter.');
    }

    $nonce = str_random(30);

    $credentials = ShopifyStoreCredentials::firstOrCreate(['store' => $shop_name], compact('nonce'));
    $url = ShopifyClient::create($shop_name)->getAuthorizeUrl($credentials->nonce);
    return redirect( $url );
  }

  public function authorise(Request $request){
    $code = '';
    $shop_name = $request->shop;

    if(!$shop_name){
      throw new Exception('Your request does not contain the shop parameter.');
    }

    // Ensure the provided hostname parameter is a valid hostname, ends with myshopify.com, and does not contain characters other than letters (a-z), numbers (0-9), dots, and hyphens.
    $credentials = ShopifyStoreCredentials::where('store', $shop_name)->first();
    if(!$credentials){
      throw new Exception('Your store is not registered with us.');
    }

    // Ensure the provided nonce is the same one that your application provided to Shopify during the Step 2: Asking for permission.
    $nonce = $request->state;
    if($credentials->nonce != $nonce){
      throw new Exception('Something went wrong trying to verify your installation.');
    }

    // Ensure the provided hmac is valid. The hmac is signed by Shopify as explained below, in the Verification section.
    //TODO hmac validation

    // The authorization code can be exchanged once for a permanent access token.
    $accessTokenData = ShopifyClient::create($shop_name)->getAccessToken($request->code);

    if(!$accessTokenData){
      throw new Exception('Access Token could not be generated. Please try again.');
    }
    //Need to consider online mode?
    //https://help.shopify.com/api/guides/authentication/oauth#api-access-modes
    $credentials->accessToken = $accessTokenData->access_token;
    $credentials->scope = $accessTokenData->scope;
    $credentials->save();

    $url = url('shopify');
    // $url = 'http://ab9e0c36.ngrok.io/shopify';

    return redirect($url);
  }

  public function settings(){
    return 'This is your settings page.';
  }

  public function help(){
    return 'Help you? Help me!';
  }

}
