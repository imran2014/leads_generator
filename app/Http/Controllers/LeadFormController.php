<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LeadForm;
use App\LeadFormField;
use Illuminate\Http\Request;

class LeadFormController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $lead_forms = LeadForm::team()->orderBy('id', 'desc')->paginate(10);

    return view('lead_forms.index', compact('lead_forms'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    $fields = $this->getFormFields();

    return view('lead_forms.create', compact('fields'));
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $lead_form = new LeadForm($request->all());
    $lead_form->save();
    $lead_form->saveFields($request->fields);

    return redirect()->route('lead_forms.index')->with('message', 'Form created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lead_form = LeadForm::findOrFail($id);

    return view('lead_forms.show', compact('lead_form'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $lead_form = LeadForm::with('fields')->findOrFail($id);
    $fields = $this->getFormFields();

    return view('lead_forms.edit', compact('lead_form', 'fields'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $lead_form = LeadForm::findOrFail($id);
    $lead_form->saveFields($request->fields);
    $lead_form->fill($request->all())->save();

    return redirect()->route('lead_forms.index')->with('message', 'Form updated successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $lead_form = LeadForm::findOrFail($id);

    if(count($lead_form->leads)){
      return back()->with('error', 'Unable to delete form, you have leads attached to this form. Please remove them first.');
    }

    $lead_form->delete();

    return redirect()->route('lead_forms.index')->with('message', 'Form deleted successfully.');
  }

  protected function getFormFields(){
    return LeadFormField::team()->get()->filter(function($field){
      return $field->field_type != 'page_referer';
    });
  }

}
