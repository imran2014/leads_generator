<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeadFormField;

class FormFieldRenderController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function view(LeadFormField $leadFormField)
  {
    $html = $leadFormField->buildHTML();

    return view('modal.default', compact('html'));
  }
}
