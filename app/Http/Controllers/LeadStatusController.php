<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\LeadStatus;
use Illuminate\Http\Request;

class LeadStatusController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $lead_statuses = LeadStatus::orderBy('id', 'desc')->paginate(10);

    return view('lead_statuses.index', compact('lead_statuses'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('lead_statuses.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $lead_status = new LeadStatus();

    $lead_status->title = $request->input("title");
    $lead_status->description = $request->input("description");

    $lead_status->save();

    return redirect()->route('lead_statuses.index')->with('message', 'Item created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lead_status = LeadStatus::findOrFail($id);

    return view('lead_statuses.show', compact('lead_status'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $lead_status = LeadStatus::findOrFail($id);

    return view('lead_statuses.edit', compact('lead_status'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $lead_status = LeadStatus::findOrFail($id);

    $lead_status->title = $request->input("title");
    $lead_status->description = $request->input("description");

    $lead_status->save();

    return redirect()->route('lead_statuses.index')->with('message', 'Item updated successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $lead_status = LeadStatus::findOrFail($id);
    $lead_status->delete();

    return redirect()->route('lead_statuses.index')->with('message', 'Item deleted successfully.');
  }

}
