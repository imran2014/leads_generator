<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeadForm;

class FormRenderController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function view(LeadForm $form)
  {
    $html = $form->buildHTML();

    return view('modal.textarea', compact('html'));
  }
}
