<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\FieldValue;
use Illuminate\Http\Request;

class FieldValueController extends Controller {

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Display a listing of the resource.
   *
   * @return Response
   */
  public function index()
  {
    $lead_form_field_values = FieldValue::orderBy('id', 'desc')->paginate(10);

    return view('lead_form_field_values.index', compact('lead_form_field_values'));
  }

  /**
   * Show the form for creating a new resource.
   *
   * @return Response
   */
  public function create()
  {
    return view('lead_form_field_values.create');
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return Response
   */
  public function store(Request $request)
  {
    $lead_form_field_value = new FieldValue();

    $lead_form_field_value->value = $request->input("value");

    $lead_form_field_value->save();

    return redirect()->route('lead_form_field_values.index')->with('message', 'Item created successfully.');
  }

  /**
   * Display the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function show($id)
  {
    $lead_form_field_value = FieldValue::findOrFail($id);

    return view('lead_form_field_values.show', compact('lead_form_field_value'));
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  int  $id
   * @return Response
   */
  public function edit($id)
  {
    $lead_form_field_value = FieldValue::findOrFail($id);

    return view('lead_form_field_values.edit', compact('lead_form_field_value'));
  }

  /**
   * Update the specified resource in storage.
   *
   * @param  int  $id
   * @param Request $request
   * @return Response
   */
  public function update(Request $request, $id)
  {
    $lead_form_field_value = FieldValue::findOrFail($id);

    $lead_form_field_value->value = $request->input("value");

    $lead_form_field_value->save();

    return redirect()->route('lead_form_field_values.index')->with('message', 'Item updated successfully.');
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param  int  $id
   * @return Response
   */
  public function destroy($id)
  {
    $lead_form_field_value = FieldValue::findOrFail($id);
    $lead_form_field_value->delete();

    return redirect()->route('lead_form_field_values.index')->with('message', 'Item deleted successfully.');
  }

}
