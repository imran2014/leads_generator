<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class Lead extends Model
{
  use HasTeamScope;

  public static function existingLead($email, LeadForm $form){
    $existing = self::where([
      'email' => $email,
      'lead_form_id' => $form->id,
      ])->first();

    if(!$existing){
      $existing = new self();
      $existing->email = $email;
      $existing->lead_form_id = $form->id;
      $existing->team_id = $form->team_id;
      $existing->save();
    }

    return $existing;
  }

  public function status(){
    return $this->belongsTo(LeadStatus::class);
  }

  public function leadForm(){
    return $this->belongsTo(LeadForm::class);
  }

  public function form(){
    return $this->leadForm()->first();
  }

  public function submissions(){
    return $this->hasMany(Submission::class);
  }
  public function values(){
    return $this->hasManyThrough(FieldValue::class, Submission::class);
  }

  public function teamSubmissions(){
    return $this->submissions()->team()->get();
  }

  public function getDisplayDateAttribute($value){
    return $this->created_at->toDayDateTimeString();
  }

  public function path(){
    return url("leads/{$this->id}");
  }
  public function fields(){
    return $this->form()->fields->sortBy('weight')->filter(function($field){
      $doNotDisplayFields = ['submit', 'email'];
      return !in_array($field->field_type, $doNotDisplayFields);
    });
  }
}
