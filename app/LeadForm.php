<?php

namespace App;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use App\Mail\NewEnquiry;
use Form;
use Mail;
use Exception;

class LeadForm extends Model
{
  use HasTeamScope;

  public $fillable = [
  'title',
  'redirect',
  'description',
  'displayable_on',
  'email_to',
  'email_cc',
  'email_bcc',
  ];

  /**
   * Syncs up the fields on lead form.
   *
   * You cannot run this until the model has been saved.
   *
   * @param  array  $field_ids
   * @return [void]
   */
  public function saveFields(array $field_ids){
    if(empty($field_ids)){
      return;
    }
    $this->createPageRefererField();
    $this->fields()->sync($field_ids);
  }

  public function addField(LeadFormField $field){
    $this->fields()->save($field);
  }

  public function leads(){
    return $this->hasMany(Lead::class);
  }

  public function submission(){
    return $this->hasMany(Submission::class);
  }

  public function ownerTeam(){
    return $this->belongsTo(Team::class, 'team_id');
  }

  public function owner(){
    return $this->belongsTo(Team::class, 'team_id')->first()->owner;
  }

  public function fields(){
    return $this->belongsToMany(LeadFormField::class, 'lead_form_lead_form_fields', 'form_id', 'form_field_id')
    ->withPivot('weight');
  }

  public function teamFields(){
    return $this->fields()->team();
  }

  public function emailField(){
    return $this->fields()->where('field_type', 'email')->first();
  }

  public function emailFieldInRequest(Request $request){
    $emailField = $this->emailField();

    if(!$emailField){
      abort(422, 'Your form does not have an email field, which is required.');
    }

    if(!$request->has($emailField->machine_name)){
      abort(422, 'Your submission does have an email value.');
    }

    return $request->{$emailField->machine_name};
  }

  public function getDisplayDateAttribute($value){
    return $this->updated_at->diffForHumans();
  }

  public function path(){
    return url("lead_forms/{$this->id}/edit");
  }

  public function mail(NewEnquiry $enquiry_mail){
    $to = $this->email_to ?: $this->owner()->email;
    $cc = explode(',', $this->email_cc);
    $bcc = explode(',', $this->email_bcc);
    $mail = Mail::to($to);

    if($this->email_cc){
      $mail->cc($cc);
    }

    if($this->email_bcc){
      $mail->bcc($bcc);
    }

    $mail->send($enquiry_mail);
  }

  public function buildHTML(){
    //TODO: test if CSRF can come from another domain
    $html[] = Form::open(['url' => $this->getFormActionURL()]);

    foreach($this->fields->sortBy('weight') as $field){
      list($label, $field) = $this->renderHTMLFactory($field->field_type)->render($field);

      //TODO: Add an option for bootstrap styling.
      if($label->toHtml() . $field->toHtml()){
        $html[] = '<div class="form-group form-item">';
        $html[] = $label->toHtml() . $field->toHtml();
        $html[] = '</div>';
      }
    }

    $html[] = '<!--Do not remove this, it is spam detection-->';
    $html[] = Form::hidden('super_required_field');
    $html[] = Form::hidden('redirect', $this->redirect);
    $html[] = Form::hidden('form_id', $this->id);
    $html[] = Form::close();

    return $html;
  }

  protected function renderHTMLFactory($type){
    $class = 'App\Autocoda\FormHtml\\' . ucfirst($type) . 'Field';

    return app($class);
  }

  protected function getFormActionURL(){
    return url('submit');
  }

  protected function createPageRefererField(){
    $refererField = LeadFormField::where('field_type', 'page_referer')->first();

    if($refererField){
      return;
    }

    $refererField = new LeadFormField;
    $refererField->machine_name = 'page_referer';
    $refererField->field_type = 'page_referer';
    $refererField->field_label = 'Page Submitted From';
    $refererField->save();
  }
}
