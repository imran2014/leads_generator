<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
  use HasTeamScope;

  public static function createNewSubmission(LeadForm $form, Lead $lead){
    $submission = new self;

    $submission->team_id = $form->team_id;
    $submission->lead_form_id = $form->id;
    $submission->lead_id = $lead->id;
    $submission->save();

    return $submission;
  }

  public function form()
  {
    return $this->belongsTo(LeadForm::class);
  }

  public function values()
  {
    return $this->hasMany(FieldValue::class);
  }

  public function lead()
  {
    return $this->belongsTo(Lead::class);
  }

  public function value($field_id){
    $value =  $this->values->where('lead_form_field_id', $field_id)->first();
    return $value ? $value->value : '-';
  }

}
