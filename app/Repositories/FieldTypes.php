<?php

namespace App\Repositories;

use Illuminate\Support\Collection;

class FieldTypes{

  protected $types = [
    'text' => 'Text Field',
    'email' => 'Email Field',
    'select' => 'Drop down',
    'checkboxes' => 'Checkboxes',
    'radiobuttons' => 'Radio buttons',
    'textarea' => 'Textarea Field',
    'number' => 'Number field',
    'page_referer' => 'Url Referrer',
    // 'url' => 'URL Field',
    // 'tel' => 'Telephone number',
    // 'date' => 'Date field',
    // 'file' => 'File',
    'submit' => 'Submit button'
  ];

  protected $requiredFields = [
    'email'
  ];

  public function get(){
    return new Collection($this->types);
  }

  public function getType($type){
    return isset($this->types[$type]) ? $this->types[$type] : null;
  }

  public function requiredFieldTypes(){
    return $this->requiredFields;
  }

  public function isRequiredFieldType($type){
    return in_array($type, $this->requiredFields) ? true : false;
  }

  public function isEmailFieldType($type){
    return $type == 'email';
  }
}
