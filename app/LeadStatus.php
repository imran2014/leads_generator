<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class LeadStatus extends Model
{
  use HasTeamScope;

  public function leads(){
    return $this->hasMany('leads');
  }

}
