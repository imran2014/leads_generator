<?php

/**
 * Gives a specific weight range array to be use on the form fields dropdown.
 *
 *  @return [array]
 */
function weightRange(){
  return array_combine(range(-50, 50), range(-50, 50));
}
