(function($){
  function showSettings(e){
    var selected = $('#field_type').val();
    if(selected === undefined){
      return;
    }

    $('#settings-panel fieldset').hide();
    var showSettings = '#' + selected + '-field';
    $(showSettings).show();
  }

  function handleSettingsFieldValues(e){
    var setting_field = $(this).parents('fieldset').find('.settings-field');
    var setting_field_obj = {};

    setting_field.each(function(key, value){
      if($(value).data('form-setting-id')){
        setting_field_obj[$(value).data('form-setting-id')] = $(value).val();
      }
      if($(value).data('form-setting-checkbox-id') && $(value).is(':checked')){
        setting_field_obj[$(value).data('form-setting-checkbox-id')] = $(value).val();
      }
    });

    $('#settings-field-value').val(JSON.stringify(setting_field_obj));
  }

  function populateSettingsField(){
    if(!$('#settings-field-value').val()){
      return;
    }
    var showSettings = '#' +  $('#field_type').val() + '-field';

    var settings = JSON.parse($('#settings-field-value').val());
    $(showSettings + ' .required-settings input[type="radio"]').each(function(key, value){
      if(settings.required !== undefined && settings.required && $(this).val()){
        $(this)[0].checked = true;
      }

      //Dynamic setting of radio buttons (not working for now)
      // if($('[data-form-setting-checkbox-id=' + value + '][value="' + settings[value] + '"]')[0] !== undefined){
        // $('[data-form-setting-checkbox-id=' + value + '][value="' + settings[value] + '"]')[0].checked = true;
      // }
    });
    $(Object.keys(settings)).each(function(key, value){
      $('[data-form-setting-id=' + value + ']').val(settings[value]);
    });
  }

  populateSettingsField();

  $('.settings-field').on('change', handleSettingsFieldValues);
  $('#field_type').on('change', showSettings);

  showSettings();
})(jQuery);
